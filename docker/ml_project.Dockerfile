FROM ubuntu:20.04
WORKDIR /ml_project

ENV PYTHONUNBUFFERED 1
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y --no-install-recommends --no-install-suggests \
	python3-pip \
	python3-wheel \
	python3-setuptools \
	curl \
	&& rm -fr /var/lib/apt/lists/*

COPY ./requirements.txt ./

RUN pip3 --disable-pip-version-check --no-cache-dir \
	install -r ./requirements.txt \
	&& rm -fr /root/.cache/pip

COPY ./ml_project/ ./

COPY ./docker/entrypoint.sh ./

ENTRYPOINT ["/ml_project/entrypoint.sh"]
