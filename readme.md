# Tinkoff case

## Установка

1) Создать в корневой директории папку ```data```
2) Скопировать туда папку ```tinkoff_hackathon_data```
3) ```docker-compose up --build``` собрать и запустить контейнер
4) ```docker-compose down``` выключить контейнер

## Функциональность

```data_research.ipynb``` - обзорная тетрадка с исследованием данных

```make_model.py``` - создание нейронной сети на Keras

```actual_xgboost.ipynb``` - создание модели градиентного бустинга XGBoost

```parse_data1.py``` - скрипт, чтобы получить numpy архив с двумя массивами - party_rk и соответсвующая соц дата

```parse_data2.py``` - скрипт, чтобы получить numpy архив с двумя массивами - соц дата и соответвующая топ категория

```parse_data3.py``` - скрипт, чтобы получить словарь пользователей в каждом из value - словарь месяц-изменение баланса и overall ключ

```visualize.py``` - скрипт, в котором две функции для визуализации финансовых результатов

В архиве ```plots.rar``` лежат графики финансовых результатов по пользователю.
