import pandas
import numpy as np
import json
data = pandas.read_csv('../data/tinkoff_hackathon_data/avk_hackathon_data_account_x_balance.csv')
# data = pandas.read_csv('dataset/test_balance.csv')
d = dict()
print(data)
progr = 1
for index, row in data.iterrows():
    if row['party_rk'] not in d.keys():
        d[row['party_rk']] = dict()
    # print(row['cur_month'])
    if 'overall' not in list(d[row['party_rk']].keys()):
        d[row['party_rk']]['overall'] = 0
    try:
        if int(row['cur_month'].split('-')[1]) not in d[row['party_rk']].keys():
            d[row['party_rk']][int(row['cur_month'].split('-')[1])] = int(row['balance_chng'])


        d[row['party_rk']]['overall'] += int(row['balance_chng'])
    except:
        pass
    if progr % 10000 == 0:
        print((progr / 614434.0)*100, "%")
    progr += 1
print(d)
#словарь пользователей в каждом из value - словарь месяц-изменение баланса и overall ключ
# temp = np.array(d)
# print(temp)
json.dump(d, open("dict_with_balances_chng.json", 'w'))
# np.savez("./dict_with_balances_chng.npz", *d)