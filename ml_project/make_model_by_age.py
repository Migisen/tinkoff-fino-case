# -*- coding: cp1251 -*-
import numpy as np
from tensorflow import keras
labels_dict = {'���/������': 0, '���������� ������': 1, '����������': 2, '�����/�������': 3, '��������': 4, '������������': 5, '�����������': 6, '������/�����': 7, '������ ������': 8, '���� ���': 9, '�������': 10, '���������': 11, '������� ������': 12, '����������� ������': 13, '��������� ������': 14, '���': 15, '����������': 16, '������': 17, '�����': 18, '������': 19, '�������': 20, '��������': 21, '���������': 22, '��������': 23, '������������': 24, '�����������': 25, '�/� ������': 26, '�����������': 27, '������ ����': 28, '�����': 29, '��������': 30, '����/�����': 31}
print(len(labels_dict))
dataset = np.load('most_valuable_category_by_soc_data.npz', allow_pickle=True)
Soc_input = (dataset['Soc_input'])
Category = list(dataset['Category'])
for i in range(len(Category)):
    Category[i] = labels_dict[Category[i]]
# Category = list(map(int, Category))
Category = np.array(Category)
print(Soc_input)
print(Category)
ages = []

for i in Soc_input:
    ages.append(i[1])

# ages = ages
# # print(ages[np.argmax(ages)])
# max_age = max(ages)
# for i in range(len(ages)):
#     ages[i] = float(ages[i]) / max_age
# print(ages)
# quit()
ages = np.array(ages)
model = keras.Sequential([
    # keras.layers.Flatten(input_shape=(50, 50)),
    keras.layers.Dense(1, activation='relu'),
    keras.layers.Dense(5, activation='relu'),
    keras.layers.Dense(15, activation='relu'),
    # keras.layers.BatchNormalization(),
    # keras.layers.Dense(75, activation='relu'),
    keras.layers.Dense(34, activation='softmax')
])
opt = keras.optimizers.Adamax(learning_rate=0.01)
model.compile(optimizer=opt, loss='sparse_categorical_crossentropy', metrics=['accuracy'])

model.fit(ages, Category, epochs=20)

model.save("model_by_age.h5")