import pandas
import numpy as np

slovar = {'nan': 0, 'Женат/замужем': 1, 'Гражданский брак': 2, 'Вдовец, вдова': 3, 'Холост/не замужем': 4, "Разведен "
                                                                                                           "(а)": 5,
          "M": 1,
          "F": 2, }
data = pandas.read_csv('../data/tinkoff_hackathon_data/avk_hackathon_data_party_x_socdem.csv')
# data = pandas.read_csv('dataset/test.csv')
# data.replace(np.nan, 'nan')
data.fillna(0)
print(data)
# print(data.set_index("party_rk").to_dict())
d = dict()

for index, row in data.iterrows():
    try:
        d[row['party_rk']] = [slovar[row['gender_cd']], int(row['age']), slovar[str(row['marital_status_desc'])],
                              row['children_cnt'],
                              row['region_flg']]
    except:
        d[row['party_rk']] = [0, int(row['age']), 0,
                              row['children_cnt'],
                              row['region_flg']]
print(d)

for i in d:
    if type(d[i]) == type('slkem'):
        d[i] = d[slovar[i]]
print(list(d.keys()))
print(list(d.values()))

np.savez("./soc_status_by_id.npz", DataX=list(d.keys()), DataY=list(d.values()))
