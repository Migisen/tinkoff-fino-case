import matplotlib.pyplot as plt
import numpy as np
import json

data = json.load(open("dict_with_balances_chng.json"))
# data = dict(np.load('./dict_with_balances_chng.npz', allow_pickle=True))
print(data)
# quit()
s_dict = dict()
for i in data.keys():
    if data[i]['overall'] not in s_dict:
        s_dict[data[i]['overall']] = 1
    else:
        s_dict[data[i]['overall']] += 1
print(s_dict)

l1 = sorted(s_dict)
l2 = []
for i in l1:
    l2.append(s_dict[i])


print(l1)
print(l2)


def plot1(l1, l2):
    plt.plot(l1, l2, color='red')
    plt.xlim([-200000, 200000])
    plt.xlabel("Рубли")
    plt.ylabel("Колличество клиентов")
    plt.title("Кол-во клиентов в зависимости от фин. результатов за 2019")

    # plt.xticks(x_pos, x)

    plt.show()

def plot2(d, party_rk):
    l1 = list(d[str(party_rk)].keys())
    l1.remove('overall')
    for i in range(len(l1)):
        l1[i] = int(l1[i])
    l1 = sorted(l1)
    print(l1)
    l2 = []
    for i in l1:
        l2.append(d[str(party_rk)][str(i)])
    print(l2)
    print(d[str(party_rk)]['overall'])
    temp = plt
    temp.bar(l1, l2)
    temp.xlabel("Месяца")
    temp.ylabel("Рубли")
    temp.title("Клиент "+ str(party_rk) + "\n Итого: " + str(d[str(party_rk)]['overall']))
    # plt.show()
    temp.savefig("./plots/" + str(party_rk) + '.png')
    # temp.up
    temp.show()


for i in data:
    print(i)
    plot2(data, int(i))
