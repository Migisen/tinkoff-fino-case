import numpy as np
from tensorflow import keras
import os

labels_dict = {'Дом/Ремонт': 0, 'Финансовые услуги': 1, 'Автоуслуги': 2, 'Связь/Телеком': 3, 'Наличные': 4, 'Супермаркеты': 5, 'Развлечения': 6, 'Одежда/Обувь': 7, 'Разные товары': 8, 'Фаст Фуд': 9, 'Топливо': 10, 'Рестораны': 11, 'Частные услуги': 12, 'Медицинские услуги': 13,
               'Сервисные услуги': 14, 'НКО': 15, 'Авиабилеты': 16, 'Аптеки': 17, 'Отели': 18, 'Музыка': 19, 'Красота': 20, 'Госсборы': 21, 'Транспорт': 22, 'Животные': 23, 'Турагентства': 24, 'Спорттовары': 25, 'Ж/д билеты': 26, 'Образование': 27, 'Аренда авто': 28, 'Цветы': 29, 'Сувениры': 30, 'Фото/Видео': 31}

dataset = np.load('./most_valuable_category_by_soc_data.npz',
                  allow_pickle=True)
soc_input = (dataset['Soc_input'])
print(len(soc_input))

category = list(dataset['Category'])
for i in range(len(category)):
    category[i] = labels_dict[category[i]]
# Category = list(map(int, Category))
category = np.array(category)
print(soc_input)
print(category)

model = keras.Sequential([
    # keras.layers.Flatten(input_shape=(50, 50)),
    keras.layers.Dense(150, input_shape=(5,), activation='relu'),
    # keras.layers.BatchNormalization(),
    keras.layers.Dense(75, activation='relu'),
    keras.layers.Dense(34, activation='softmax')
])
opt = keras.optimizers.Adam(learning_rate=0.001)
model.compile(optimizer=opt, loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

model.fit(soc_input, category, epochs=20)

# model.save("model.h5")
