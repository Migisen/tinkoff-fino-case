import pandas
import numpy as np

data = pandas.read_csv('../data/tinkoff_hackathon_data/test_transactions.csv')
print(1)
# data = pandas.read_csv('dataset/avk_hackathon_data_train_transactions.csv')
print(2)
d = dict()
# temp_d = dict()
inc = 0
for index, row in data.iterrows():
    if row['party_rk'] not in d.keys():
        d[row['party_rk']] = dict()

    if row['category'] not in d[row['party_rk']].keys():
        d[row['party_rk']][row['category']] = 0
    d[row['party_rk']][row['category']] += row['transaction_amt_rur']
print(3)
# for i in d:
#     if d[i] not in temp_d.keys():
#         temp_d[d[i]] = inc
#         inc += 1
d2 = dict()
d3 = dict()
for i in d:
    d2 = {k: v for k, v in sorted(d[i].items(), key=lambda item: item[1])}
    # print(d2[list(d2.keys())[-1]], list(d2.keys())[-1])
    d3[i] = list(d2.keys())[-1]
# print(temp_d)
# for i in d:
#     d[i] = temp_d[d[i]]
# print(d)
# print(d3)
#
# print(list(d3.keys()))
# print(list(d3.values()))
print(d3)

for i in d3:
    print(str(d3[i]))
    if str(d3[i]) == "nan":
        d3[i] = ""

d3 = {key: val for key, val in d3.items() if val != ""}
print(d3)
dataset = np.load('soc_status_by_id.npz', allow_pickle=True)
my1 = dataset['DataX']
my2 = dataset['DataY']
print(my1)
print(my2)
soc_input = []
result = []
for i in range(len(my1)):
    if my1[i] in d3.keys():
        soc_input.append(list(my2[i]))
        result.append(d3[my1[i]])
print(soc_input)
print(result)
temp = 0
labels_dict = dict()
for i in result:
    if i not in labels_dict.keys():
        labels_dict[i] = temp
        temp += 1
# print(labels_dict)
print(d3)
# np.savez("./most_valuable_category_by_soc_data.npz", Soc_input=soc_input, Category=list(d3.values()))
